$(document).ready(function() {
    $("#button-add-attachment").click(function() {
	var new_id = parseInt($("#attachments").attr("data-next-id"));
	var to_append = dynInputWrapper('<input name="attachment-' + new_id + '" type="file" class="form-control"><span class="input-group-text"><i class="bi-chevron-expand"></i></span><button type="button" class="btn btn-danger button-delete-attachment"><i class="bi-trash"></i></button>');
	$("#attachments").append(to_append);
	$("#attachments").attr("data-next-id", new_id + 1);
    });

    $("#button-add-text").click(function() {
	var new_id = parseInt($("#attachments").attr("data-next-id"));
	var to_append = dynInputWrapper('<textarea name="attachment-' + new_id + '" class="form-control" rows="5"></textarea><span class="input-group-text"><i class="bi-chevron-expand"></i></span><button type="button" class="btn btn-danger button-delete-attachment"><i class="bi-trash"></i></button>');
	$("#attachments").append(to_append);
	$("#attachments").attr("data-next-id", new_id + 1);
    });

    $("#button-search-authors").click(showModal);

    $("#button-select-author").click(function() {
	var author_id = $("#select-authors option:selected").val();
	var author_name = $("#select-authors option:selected").text();
	$("#author-id").val(author_id);
	$("#author-name").val(author_name);
	$("#author-query").val("");
	$("#modal-author-select").modal("hide");
    });

    $("#attachments").sortable({
	group: "list",
	animation: 200,
    });

    $("body").on("click", ".button-delete-attachment", function() {
	$(this).closest(".attachment-item").remove();
    });
});

function dynInputWrapper(inside) {
    return '<div class="row my-3 attachment-item"><div class="col"><div class="input-group">' + inside + '</div></div></div>';
}

function showModal() {
    var query = $("#author-query").val();
    $.ajax({
	url: "/author/search",
	type: "POST",
	contentType: "application/json",
	data: JSON.stringify({ "query": query }),
	success: function(results) {
	    var sa = $("#select-authors");
	    sa.html("");
	    sa.prop("disabled", results.results.length == 0);
	    if (results.results.length > 0) {
		sa.prop("disabled", false);
		$("#button-select-author").prop("disabled", false);
	    } else {
		sa.prop("disabled", true);
		$("#button-select-author").prop("disabled", true);
		sa.append("<option>No results</option>");
	    }
	    for (result of results.results) {
		sa.append("<option value=\"" + result.id + "\">" + result.name + " (" + result.id + ")</option>");
	    }
	    $("#modal-author-select").modal("show");
	},
    })
}

function checkActive() {
    var author_search_focused = $("#author-query").is(":focus");
    if (author_search_focused) {
	showModal();
	return false;
    } else {
	return true;
    }
}
