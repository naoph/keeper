use std::io::Write;

use bytes::{Buf, BufMut};
use futures::TryStreamExt;
use sha2::Digest;
use snafu::{ResultExt, Snafu};
use warp::multipart::{FormData, Part};

use crate::models::{AttKind, InsEntry};
use crate::MEDIA_DIR;

#[derive(Debug)]
pub struct IncompleteEntry {
    pub ins_entry: InsEntry,
    pub tag_vec: Vec<String>,
    pub att_vec: Vec<IncompleteAttachment>,
}

#[derive(Debug)]
pub struct IncompleteAttachment {
    pub position: i32,
    pub filename: String,
    pub kind: AttKind,
    pub orig_name: Option<String>,
}

pub async fn process_new_entry(mut form: FormData) -> Result<IncompleteEntry, ProcessNewEntryError> {
    // Extract and process non-attachment fields in known order
    let title = extract_field(&mut form, "title").await?.nullify();
    let source_url = extract_field(&mut form, "source-url").await?.nullify();
    let source_is_orig = match &extract_field(&mut form, "source-type").await?[..] {
        "original" => true,
        "non-original" => false,
        s => return Err(ProcessNewEntryError::InvalidFieldValue { field: "source-type".to_string(), value: s.to_string() }),
    };
    let source_is_orig = if source_url.is_some() {
        Some(source_is_orig)
    } else {
        None
    };
    let comment = extract_field(&mut form, "comment").await?.nullify();
    let tags_string = extract_field(&mut form, "tags").await?;
    let tags = crate::process_tags(tags_string);
    let author_id = extract_field(&mut form, "author-id").await?.nullify();
    let author_id = match author_id {
        None => None,
        Some(s) => Some(s.parse::<i32>().context(ConvertToIntSnafu { field: "author-id".to_string(), value: s })?),
    };

    // Construct new entry
    let ins_entry = InsEntry {
        title,
        source: source_url,
        comment,
        thumbnail: None,
        author: author_id,
        source_is_orig,
    };

    // Process attachments
    let mut att_incr = 1;
    let mut atts = Vec::new();
    while let Ok(Some(p)) = form.try_next().await {
        let new_att = process_new_attachment(p, att_incr).await?;
        atts.push(new_att);
        att_incr += 1;
    }

    Ok(IncompleteEntry {
        ins_entry,
        tag_vec: tags,
        att_vec: atts,
    })
}

trait Nullify: Default + PartialEq {
    /// Turn `"text"` into `Some("text")` and `""` into `None`
    fn nullify(self) -> Option<Self> {
        if self == Self::default() {
            return None;
        }
        Some(self)
    }
}

impl<T: Default + PartialEq> Nullify for T {}

async fn process_new_attachment(mut part: Part, position: i32) -> Result<IncompleteAttachment, ProcessNewEntryError> {
    // Collect some basic info from the Part
    let (att_kind, extension) = match part.content_type() {
        None => (AttKind::Text, "txt"),
        Some("image/png") => (AttKind::Image, "png"),
        Some("image/jpeg") => (AttKind::Image, "jpg"),
        Some("image/webp") => (AttKind::Image, "webp"),
        Some("image/avif") => (AttKind::Image, "avif"),
        Some("image/gif") => (AttKind::Image, "gif"),
        Some("video/mp4") => (AttKind::Video, "mp4"),
        Some("video/webm") => (AttKind::Video, "webm"),
        Some(k) => return Err(ProcessNewEntryError::UnsupportedFile { name: part.name().to_string(), kind: k.to_string() }),
    };
    let orig_name = part.filename().map(|s| s.to_string());

    // Create a temp file and a Sha256 instance
    let mut temp_file = tempfile::NamedTempFile::new().context(OpenTempFileSnafu {})?;
    let mut hasher = sha2::Sha256::new();

    // Write each chunk to disc, hashing as we go
    while let Some(chunk_res) = part.data().await {
        match chunk_res {
            Ok(mut chunk) => {
                let b = chunk.copy_to_bytes(chunk.remaining());
                let _ = temp_file.write(&b).context(WriteTempFileSnafu {})?;
                hasher.update(&b);
            }
            Err(e) => {
                return Err(e).context(GetChunkSnafu {});
            }
        };
    }

    // Finalize hashing, copy & delete temp file
    let sha256sum = hasher.finalize();
    let destination_base = format!("{}.{}", hex::encode(sha256sum), extension);
    let destination = MEDIA_DIR.join(&destination_base);
    std::fs::copy(temp_file.path(), &destination).context(MoveToMediaSnafu {})?;
    let _ = temp_file.close();
    
    Ok(IncompleteAttachment {
        position,
        filename: destination_base,
        kind: att_kind,
        orig_name,
    })
}

/// Extract the first Part from a multipart form, verifying its name matches our expectations
async fn extract_field(form: &mut FormData, expected_name: &str) -> Result<String, ProcessNewEntryError> {
    let part = match form.try_next().await {
        Ok(Some(p)) => p,
        _ => return Err(ProcessNewEntryError::MissingField { field_name: expected_name.to_string() }),
    };

    if part.name() != expected_name {
        return Err(ProcessNewEntryError::IncorrectField { expected: expected_name.to_string(), found: part.name().to_string() });
    }

    // Collect all bytes
    let raw_bytes = part.stream()
        .try_fold(Vec::new(), |mut vec, data| {
            vec.put(data);
            async move { Ok(vec) }
        })
        .await
        .context(FoldStreamSnafu {})?;

    Ok(String::from_utf8(raw_bytes).context(ConvertToStringSnafu {})?)
}

#[derive(Debug, Snafu)]
pub enum ProcessNewEntryError {
    #[snafu(display("Missing field: {}", field_name))]
    MissingField {
        field_name: String,
    },

    #[snafu(display("Incorrect field: expected `{}`, found `{}`", expected, found))]
    IncorrectField {
        expected: String,
        found: String,
    },

    #[snafu(display("Invalid value `{}` for field `{}`", field, value))]
    InvalidFieldValue {
        field: String,
        value: String,
    },

    #[snafu(display("Failed to fold stream contents: {}", source))]
    FoldStream {
        source: warp::Error,
    },

    #[snafu(display("Failed to convert bytes to string: {}", source))]
    ConvertToString {
        source: std::string::FromUtf8Error,
    },

    #[snafu(display("Numerical field {} had invalid value {}", field, value))]
    ConvertToInt {
        source: std::num::ParseIntError,
        field: String,
        value: String,
    },

    #[snafu(display("Unsupported MIME {} on file {}", kind, name))]
    UnsupportedFile {
        name: String,
        kind: String,
    },

    #[snafu(display("Error opening temp file: {}", source))]
    OpenTempFile {
        source: std::io::Error,
    },

    #[snafu(display("Error writing temp file: {}", source))]
    WriteTempFile {
        source: std::io::Error,
    },

    #[snafu(display("Error getting attachment chunk: {}", source))]
    GetChunk {
        source: warp::Error,
    },

    #[snafu(display("Error moving attachment to media directory: {}", source))]
    MoveToMedia {
        source: std::io::Error,
    },
}

impl ProcessNewEntryError {
    pub fn http_status(&self) -> http::StatusCode {
        match self {
            &ProcessNewEntryError::MissingField { field_name: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::IncorrectField { expected: _, found: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::InvalidFieldValue { field: _, value: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::FoldStream { source: _ } => http::StatusCode::INTERNAL_SERVER_ERROR,
            &ProcessNewEntryError::ConvertToString { source: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::ConvertToInt { source: _, field: _, value: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::UnsupportedFile { name: _, kind: _ } => http::StatusCode::BAD_REQUEST,
            &ProcessNewEntryError::OpenTempFile { source: _ } => http::StatusCode::INTERNAL_SERVER_ERROR,
            &ProcessNewEntryError::WriteTempFile { source: _ } => http::StatusCode::INTERNAL_SERVER_ERROR,
            &ProcessNewEntryError::GetChunk { source: _ } => http::StatusCode::INTERNAL_SERVER_ERROR,
            &ProcessNewEntryError::MoveToMedia { source: _ } => http::StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}
