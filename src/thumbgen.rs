use std::fs::read_to_string;
use std::path::PathBuf;

use diesel::prelude::*;
use snafu::{ResultExt, Snafu};

use crate::models::{AttKind, DbEntry};
use crate::{MEDIA_DIR, THUMB_SIZE};

/// Generate a thumbnail for a given entry
pub fn gen_thumb(entry: &DbEntry, conn: &PgConnection) -> Result<(), ThumbnailError> {
    // Load the entry's first attachment
    let attachment = entry.first_attachment(conn).context(LoadingAttachmentSnafu {})?;

    // Generate
    let thumb_stub = match attachment.kind {
        AttKind::Text => gen_text_thumb(attachment.filename),
        AttKind::Image => gen_image_thumb(attachment.filename),
        AttKind::Video => gen_video_thumb(attachment.filename),
    }?;

    // Update the entry's thumbnail in the database
    entry.set_thumbnail(conn, thumb_stub).context(UpdatingDatabaseSnafu {})?;

    Ok(())
}

/// Generate thumbnail for image attachments
fn gen_image_thumb(full_image: String) -> Result<Option<String>, ThumbnailError> {
    // Create a square white canvas upon which to overlay the thumbnail
    let mut canvas = image::ImageBuffer::new(THUMB_SIZE, THUMB_SIZE);
    for (_, _, px) in canvas.enumerate_pixels_mut() {
        *px = image::Rgba([255, 255, 255, 255]);
    }

    // Load & shrink the original image
    let orig_path = MEDIA_DIR.join(&full_image);
    let orig = image::open(&orig_path).context(OpeningImageSnafu { img_path: orig_path.to_string_lossy() })?;
    let thumb = orig.thumbnail(THUMB_SIZE, THUMB_SIZE);

    // Overlay the two
    let offset_x = (THUMB_SIZE - thumb.width()) / 2;
    let offset_y = (THUMB_SIZE - thumb.height()) / 2;
    image::imageops::overlay(&mut canvas, &thumb, offset_x as i64, offset_y as i64);

    // Write to media directory
    let stub = PathBuf::new().join("thumbnails").join(format!("{}.thumb.jpg", &full_image));
    let destination = MEDIA_DIR.join(&stub);
    canvas.save(&destination).context(WriteThumbSnafu {})?;

    Ok(Some(stub.to_string_lossy().to_string()))
}

/// Generate thumbnail for video attachments
fn gen_video_thumb(video: String) -> Result<Option<String>, ThumbnailError> {
    // Set up temp file for frame output
    let frame_file = tempfile::Builder::new()
        .suffix(".jpg")
        .tempfile()
        .context(OpenTempFileSnafu {})?;
    let frame_path = frame_file.path();

    // Get a frame from 5 seconds in
    let orig_path = MEDIA_DIR.join(&video);
    run_cmd!(ffmpeg -ss 00:00:05 -i $orig_path -frames:v 1 -q:v 5 -y $frame_path).context(ExtractFrameSnafu {})?;

    // Create a square white canvas upon which to overlay the thumbnail
    let mut canvas = image::ImageBuffer::new(THUMB_SIZE, THUMB_SIZE);
    for (_, _, px) in canvas.enumerate_pixels_mut() {
        *px = image::Rgba([255, 255, 255, 255]);
    }

    // Load & shrink the extracted frame
    let orig = image::open(frame_path).context(OpeningImageSnafu { img_path: orig_path.to_string_lossy() })?;
    match frame_file.close() {
        Ok(_) => {},
        Err(e) => error!("Failed to close temp file: {}", e),
    };
    let thumb = orig.thumbnail(THUMB_SIZE, THUMB_SIZE);

    // Overlay the two
    let offset_x = (THUMB_SIZE - thumb.width()) / 2;
    let offset_y = (THUMB_SIZE - thumb.height()) / 2;
    image::imageops::overlay(&mut canvas, &thumb, offset_x as i64, offset_y as i64);

    // Write to media directory
    let stub = PathBuf::new().join("thumbnails").join(format!("{}.thumb.jpg", &video));
    let destination = MEDIA_DIR.join(&stub);
    canvas.save(&destination).context(WriteThumbSnafu {})?;

    Ok(Some(stub.to_string_lossy().to_string()))
}

/// Generate thumbnail for text attachments
fn gen_text_thumb(text_path: String) -> Result<Option<String>, ThumbnailError> {
    // Load the text & trim to reasonable length
    let orig_path = MEDIA_DIR.join(&text_path);
    let orig_text = read_to_string(&orig_path).context(OpenTextSnafu { text_path: text_path.clone() })?;
    let trimmed_text = match orig_text.char_indices().nth(400) {
        None => orig_text,
        Some((idx, _)) => (&orig_text[..idx]).to_string(),
    };

    // Set up temp file for imagemagick output
    let im_file = tempfile::Builder::new()
        .suffix(".png")
        .tempfile()
        .context(OpenTempFileSnafu {})?;
    let im_path = im_file.path();

    // Run imagemagick
    let size = {
        let without_margin = THUMB_SIZE - 20;
        format!("{}x{}", without_margin, without_margin)
    };
    let caption = format!("caption:{}", trimmed_text);
    run_cmd!(convert -background white -fill black -font Lato-Regular -pointsize 19 -size $size $caption $im_path).context(RunImageMagickSnafu {})?;

    // Create a square white canvas upon which to overlay the thumbnail
    let mut canvas = image::ImageBuffer::new(THUMB_SIZE, THUMB_SIZE);
    for (_, _, px) in canvas.enumerate_pixels_mut() {
        *px = image::Rgba([255, 255, 255, 255]);
    }

    // Load the margin-less image output by imagemagick
    let nomargin = image::open(im_path).context(OpeningImageSnafu { img_path: im_path.to_string_lossy() })?;
    match im_file.close() {
        Ok(_) => {},
        Err(e) => error!("Failed to close temp file: {}", e),
    };

    // Overlay the two
    image::imageops::overlay(&mut canvas, &nomargin, 10, 10);

    // Write to media directory
    let stub = PathBuf::new().join("thumbnails").join(format!("{}.thumb.jpg", &text_path));
    let destination = MEDIA_DIR.join(&stub);
    canvas.save(&destination).context(WriteThumbSnafu {})?;

    Ok(Some(stub.to_string_lossy().to_string()))
}

#[derive(Debug, Snafu)]
pub enum ThumbnailError {
    #[snafu(display("Error loading attachment from database: {}", source))]
    LoadingAttachment {
        source: diesel::result::Error,
    },

    #[snafu(display("Error opening image {} for thumbnail generation: {}", img_path, source))]
    OpeningImage {
        source: image::ImageError,
        img_path: String,
    },

    #[snafu(display("Error writing thumbnail: {}", source))]
    WriteThumb {
        source: image::ImageError,
    },

    #[snafu(display("Error updating thumbnail in database: {}", source))]
    UpdatingDatabase {
        source: diesel::result::Error,
    },

    #[snafu(display("Error opening temp file: {}", source))]
    OpenTempFile {
        source: std::io::Error,
    },

    #[snafu(display("Error extracting frame: {}", source))]
    ExtractFrame {
        source: std::io::Error,
    },

    #[snafu(display("Error opening text attachment {}: {}", text_path, source))]
    OpenText {
        source: std::io::Error,
        text_path: String,
    },

    #[snafu(display("Error running imagemagick: {}", source))]
    RunImageMagick {
        source: std::io::Error,
    },
}

/// Placeholder function for attachment kinds not yet implemented
fn _skip_gen_thumb() -> Result<Option<String>, ThumbnailError> {
    Ok(None)
}
