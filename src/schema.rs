table! {
    attachments (id) {
        id -> Int4,
        entry -> Int4,
        position -> Int4,
        filename -> Text,
        kind -> crate::models::AttKindMapping,
        orig_name -> Nullable<Text>,
    }
}

table! {
    authors (id) {
        id -> Int4,
        name -> Text,
        namespace -> Int4,
    }
}

table! {
    collection_links (id) {
        id -> Int4,
        entry -> Int4,
        collection -> Int4,
        position -> Int4,
    }
}

table! {
    collections (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    entries (id) {
        id -> Int4,
        title -> Nullable<Text>,
        source -> Nullable<Text>,
        comment -> Nullable<Text>,
        thumbnail -> Nullable<Text>,
        author -> Nullable<Int4>,
        source_is_orig -> Nullable<Bool>,
    }
}

table! {
    namespaces (id) {
        id -> Int4,
        name -> Text,
    }
}

table! {
    sessions (id) {
        id -> Int4,
        token -> Text,
        userid -> Int4,
    }
}

table! {
    tag_links (id) {
        id -> Int4,
        entry -> Int4,
        tag -> Text,
    }
}

table! {
    tags (name) {
        name -> Text,
        description -> Nullable<Text>,
    }
}

table! {
    users (id) {
        id -> Int4,
        username -> Text,
        passhash -> Text,
        permission -> crate::models::PermissionLevelMapping,
    }
}

table! {
    entries_with_tag_array (id) {
        id -> Int4,
        title -> Nullable<Text>,
        source -> Nullable<Text>,
        comment -> Nullable<Text>,
        thumbnail -> Nullable<Text>,
        author -> Nullable<Int4>,
        source_is_orig -> Nullable<Bool>,
        tag_list -> Array<Text>,
    }
}

table! {
    tag_stats (tag) {
        tag -> Text,
        occurrences -> Int8,
    }
}

allow_tables_to_appear_in_same_query!(
    attachments,
    authors,
    collection_links,
    collections,
    entries,
    namespaces,
    sessions,
    tag_links,
    tags,
    users,
);

joinable!(sessions -> users (userid));
joinable!(collection_links -> collections (collection));
joinable!(collection_links -> entries (entry));
joinable!(entries -> authors (author));
joinable!(authors -> namespaces (namespace));
