use diesel::prelude::*;
use diesel_derive_enum::DbEnum;
use serde::Serialize;

use super::schema::*;

// schema.rs: kind -> crate::models::AttKindMapping,
#[derive(DbEnum, Debug, Serialize)]
pub enum AttKind {
    Image,
    Video,
    Text,
}

#[derive(Clone, DbEnum, Debug, PartialEq, Serialize)]
pub enum PermissionLevel {
    Read,
    Write,
    Admin,
}

impl PermissionLevel {
    pub fn allowed_write(&self) -> bool {
        match self {
            PermissionLevel::Admin | PermissionLevel::Write => true,
            _ => false,
        }
    }

    pub fn allowed_admin(&self) -> bool {
        match self {
            PermissionLevel::Admin => true,
            _ => false,
        }
    }

    pub fn from_string<T: ToString>(string: T) -> Result<PermissionLevel, ()> {
        match &string.to_string()[..] {
            "read" | "Read" => Ok(PermissionLevel::Read),
            "write" | "Write" => Ok(PermissionLevel::Write),
            "admin" | "Admin" => Ok(PermissionLevel::Admin),
            _ => Err(()),
        }
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbAttachment {
    pub id: i32,
    pub entry: i32,
    pub position: i32,
    pub filename: String,
    pub kind: AttKind,
    pub orig_name: Option<String>,
}

#[derive(Debug, Insertable)]
#[table_name = "attachments"]
pub struct InsAttachment {
    pub entry: i32,
    pub position: i32,
    pub filename: String,
    pub kind: AttKind,
    pub orig_name: Option<String>,
}

impl InsAttachment {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbAttachment, diesel::result::Error> {
        diesel::insert_into(attachments::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbAuthor {
    pub id: i32,
    pub name: String,
    pub namespace: i32,
}

impl DbAuthor {
    /// List authors whose name contains a string (case-insensitive), beginning with exact matches
    pub fn with_name_like(conn: &PgConnection, query: &str) -> Result<Vec<DbAuthor>, diesel::result::Error> {
        // Get exact matches
        let mut results = authors::table
            .filter(authors::name.ilike(query))
            .load::<DbAuthor>(conn)?;

        // Get fuzzy matches
        let mut fuzzy_results = authors::table
            .filter(authors::name.ilike(format!("%{}%", query)))
            .filter(authors::name.not_ilike(&query))
            .load::<DbAuthor>(conn)?;

        // Merge
        results.append(&mut fuzzy_results);

        Ok(results)
    }
}

#[derive(Debug, Insertable)]
#[table_name = "authors"]
pub struct InsAuthor {
    pub name: String,
    pub namespace: i32,
}

impl InsAuthor {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbAuthor, diesel::result::Error> {
        diesel::insert_into(authors::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable)]
pub struct DbCollectionLink {
    pub id: i32,
    pub entry: i32,
    pub collection: i32,
    pub position: i32,
}

#[derive(Debug, Insertable)]
#[table_name = "collection_links"]
pub struct InsCollectionLink {
    pub entry: i32,
    pub collection: i32,
    pub position: i32,
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbCollection {
    pub id: i32,
    pub name: String,
}

impl DbCollection {
    /// Load specific collection by its id
    pub fn by_id(conn: &PgConnection, id: i32) -> Result<DbCollection, diesel::result::Error> {
        collections::table.find(id)
            .get_result(conn)
    }
}

#[derive(Debug, Insertable)]
#[table_name = "collections"]
pub struct InsCollection {
    pub name: String,
}

impl InsCollection {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbCollection, diesel::result::Error> {
        diesel::insert_into(collections::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbEntry {
    pub id: i32,
    pub title: Option<String>,
    pub source: Option<String>,
    pub comment: Option<String>,
    pub thumbnail: Option<String>,
    pub author: Option<i32>,
    pub source_is_orig: Option<bool>,
}

impl DbEntry {
    /// Load specific entry by its id
    pub fn by_id(conn: &PgConnection, id: i32) -> Result<DbEntry, diesel::result::Error> {
        entries::table.find(id)
            .get_result(conn)
    }

    /// Load a single random entry
    pub fn random(conn: &PgConnection) -> Result<DbEntry, diesel::result::Error> {
        no_arg_sql_function!(RANDOM, (), "Represents the sql RANDOM() function");

        entries::table
            .order(RANDOM)
            .first(conn)
    }

    /// Get first attachment belonging to this entry, primarily for thumbnail generation
    pub fn first_attachment(&self, conn: &PgConnection) -> Result<DbAttachment, diesel::result::Error> {
        let attachment: DbAttachment = attachments::table
            .filter(attachments::entry.eq(self.id))
            .filter(attachments::position.eq(1))
            .first(conn)?;
        Ok(attachment)
    }

    /// Set the entry's thumbnail path
    pub fn set_thumbnail(&self, conn: &PgConnection, path: Option<String>) -> Result<bool, diesel::result::Error> {
        let target = entries::table.find(self.id);
        diesel::update(target)
            .set(entries::thumbnail.eq(path))
            .execute(conn)
            .map(|rows: usize| rows > 0)
    }

    // Get the entry's author and their respective namespace
    pub fn get_author_info(&self, conn: &PgConnection) -> Result<Option<(DbAuthor, DbNamespace)>, diesel::result::Error> {
        let author_id = match self.author {
            None => return Ok(None),
            Some(a) => a,
        };
        let out = authors::table.inner_join(namespaces::table)
            .filter(authors::id.eq(author_id))
            .first::<(DbAuthor, DbNamespace)>(conn)?;

        Ok(Some(out))
    }
}

#[derive(Debug, Insertable)]
#[table_name = "entries"]
pub struct InsEntry {
    pub title: Option<String>,
    pub source: Option<String>,
    pub comment: Option<String>,
    pub thumbnail: Option<String>,
    pub author: Option<i32>,
    pub source_is_orig: Option<bool>,
}

impl InsEntry {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbEntry, diesel::result::Error> {
        diesel::insert_into(entries::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbNamespace {
    pub id: i32,
    pub name: String,
}

impl DbNamespace {
    /// Load specific namespace by its id
    pub fn by_id(conn: &PgConnection, id: i32) -> Result<DbNamespace, diesel::result::Error> {
        namespaces::table.find(id)
            .get_result(conn)
    }
}

#[derive(Debug, Insertable)]
#[table_name = "namespaces"]
pub struct InsNamespace {
    pub name: String,
}

impl InsNamespace {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbNamespace, diesel::result::Error> {
        diesel::insert_into(namespaces::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable)]
pub struct DbSession {
    pub id: i32,
    pub token: String,
    pub userid: i32,
}

#[derive(Debug, Insertable)]
#[table_name = "sessions"]
pub struct InsSession {
    pub token: String,
    pub userid: i32,
}

impl InsSession {
    /// Create a new session for a given user
    pub fn new(userid: i32) -> InsSession {
        use rand::Rng;

        let token_bytes = rand::thread_rng().gen::<[u8; 24]>();
        let token = base64::encode(token_bytes);

        InsSession {
            token,
            userid,
        }
    }
}

#[derive(Debug, Queryable)]
pub struct DbTagLink {
    pub id: i32,
    pub entry: i32,
    pub tag: String,
}

#[derive(Debug, Insertable)]
#[table_name = "tag_links"]
pub struct InsTagLink {
    pub entry: i32,
    pub tag: String,
}

impl InsTagLink {
    pub fn insert(&self, conn: &PgConnection) -> Result<DbTagLink, diesel::result::Error> {
        diesel::insert_into(tag_links::table)
            .values(self)
            .get_result(conn)
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbTag {
    pub name: String,
    pub description: Option<String>,
}

impl DbTag {
    /// Load specific tag by its name
    pub fn by_name(conn: &PgConnection, name: &String) -> Result<DbTag, diesel::result::Error> {
        tags::table.find(name)
            .get_result(conn)
    }

    pub fn set_description(&self, conn: &PgConnection, new_description: Option<String>) -> Result<(), diesel::result::Error> {
        let target = tags::table
            .find(&self.name);
        let result = diesel::update(target)
            .set(tags::description.eq(new_description))
            .execute(conn);

        match result {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }
}

#[derive(Debug, Insertable)]
#[table_name = "tags"]
pub struct InsTag {
    pub name: String,
    pub description: Option<String>,
}

impl InsTag {
    pub fn insert_or_not(&self, conn: &PgConnection) -> Result<bool, diesel::result::Error> {
        diesel::insert_into(tags::table)
            .values(self)
            .on_conflict_do_nothing()
            .execute(conn)
            .map(|i| i > 0)
    }
}

#[derive(Clone, Debug, Queryable)]
pub struct DbUser {
    pub id: i32,
    pub username: String,
    pub passhash: String,
    pub permission: PermissionLevel,
}

impl DbUser {
    pub fn check_password(&self, password: &String) -> bool {
        use bcrypt::verify;

        match verify(password, &self.passhash) {
            Ok(true) => true,
            _ => false,
        }
    }

    pub fn allowed_write(&self) -> bool {
        self.permission.allowed_write()
    }

    pub fn allowed_admin(&self) -> bool {
        self.permission.allowed_admin()
    }
}

#[derive(Debug, Insertable)]
#[table_name = "users"]
pub struct InsUser {
    pub username: String,
    pub passhash: String,
    pub permission: PermissionLevel,
}

impl InsUser {
    pub fn create(username: impl ToString, password: impl ToString, permission: PermissionLevel) -> InsUser {
        use bcrypt::{DEFAULT_COST, hash};

        let username = username.to_string();
        let passhash = hash(&password.to_string(), DEFAULT_COST).unwrap();

        InsUser {
            username,
            passhash,
            permission,
        }
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbEntryWithTagArray {
    pub id: i32,
    pub title: Option<String>,
    pub source: Option<String>,
    pub comment: Option<String>,
    pub thumbnail: Option<String>,
    pub author: Option<i32>,
    pub source_is_orig: Option<bool>,
    pub tag_list: Vec<String>,
}

impl DbEntryWithTagArray {
    pub fn to_pair(self) -> (DbEntry, Vec<String>) {
        let db_entry = DbEntry {
            id: self.id,
            title: self.title,
            source: self.source,
            comment: self.comment,
            thumbnail: self.thumbnail,
            author: self.author,
            source_is_orig: self.source_is_orig,
        };

        (db_entry, self.tag_list)
    }
}

#[derive(Debug, Queryable, Serialize)]
pub struct DbTagStat {
    pub tag: String,
    pub occurrences: i64,
}

impl DbTagStat {
    /// Load specific tag stat by its name
    pub fn _by_name(conn: &PgConnection, name: &String) -> Result<DbTagStat, diesel::result::Error> {
        tag_stats::table.find(name)
            .get_result(conn)
    }

    /// Load all tag stats, sorted by occurrences
    pub fn all(conn: &PgConnection) -> Result<Vec<DbTagStat>, diesel::result::Error> {
        tag_stats::table
            .order(tag_stats::occurrences.desc())
            .load(conn)
    }
}
