use serde::Serialize;

use crate::models::*;

#[derive(Debug, Serialize)]
pub struct SearchAuthor {
    pub results: Vec<DbAuthor>,
}
