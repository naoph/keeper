use serde::Deserialize;

#[derive(Debug, Deserialize)]
pub struct SearchAuthor {
    pub query: String,
}
