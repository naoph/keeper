use serde::Deserialize;
use snafu::Snafu;

use super::models;

#[derive(Debug, Deserialize)]
pub struct Login {
    username: Option<String>,
    password: Option<String>,
}

impl Login {
    pub fn unpack(self) -> Result<(String, String), InvalidForm> {
        Ok((
            self.username.ok_or(InvalidForm::missing_field("Username"))?,
            self.password.ok_or(InvalidForm::missing_field("Password"))?,
        ))
    }
}

#[derive(Debug, Deserialize)]
pub struct AuthorCreate {
    author_name: Option<String>,
    author_namespace: Option<String>,
}

impl AuthorCreate {
    pub fn unpack(self) -> Result<(String, i32), InvalidForm> {
        let author_namespace = self.author_namespace.ok_or(InvalidForm::missing_field("Namespace"))?;
        let author_namespace = author_namespace.parse::<i32>()
            .or(Err(InvalidForm::invalid_value("Namespace")))?;
        Ok((
            self.author_name.ok_or(InvalidForm::missing_field("Author name"))?,
            author_namespace,
        ))
    }
}

#[derive(Debug, Deserialize)]
pub struct UserUpdate {
    username: Option<String>,
    permissions: Option<String>,
}

impl UserUpdate {
    pub fn unpack(self) -> Result<(String, models::PermissionLevel), InvalidForm> {
        let permissions = self.permissions.ok_or(InvalidForm::missing_field("Permissions"))?;
        let permissions = models::PermissionLevel::from_string(permissions)
            .or(Err(InvalidForm::invalid_value("Permissions")))?;
        Ok((
            self.username.ok_or(InvalidForm::missing_field("Username"))?,
            permissions,
        ))
    }
}

#[derive(Debug, Deserialize)]
pub struct NamespaceCreate {
    name: Option<String>,
}

impl NamespaceCreate {
    pub fn unpack(self) -> Result<String, InvalidForm> {
        Ok(self.name.ok_or(InvalidForm::missing_field("New namespace"))?)
    }
}

#[derive(Debug, Deserialize)]
pub struct NewCollection {
    collection_name: Option<String>,
}

impl NewCollection {
    pub fn unpack(self) -> Result<String, InvalidForm> {
        Ok(self.collection_name.ok_or(InvalidForm::missing_field("Collection name"))?)
    }
}

#[derive(Debug, Deserialize)]
pub struct AppendCollection {
    entry_id: Option<String>,
}

impl AppendCollection {
    pub fn unpack(self) -> Result<i32, InvalidForm> {
        let entry_id = self.entry_id.ok_or(InvalidForm::missing_field("Entry ID"))?;
        let entry_id = entry_id.parse::<i32>()
            .or(Err(InvalidForm::invalid_value("Entry ID")))?;
        Ok(entry_id)
    }
}

#[derive(Debug, Deserialize)]
pub struct UpdateTag {
    description: Option<String>,
}

impl UpdateTag {
    pub fn unpack(self) -> Result<String, InvalidForm> {
        Ok(self.description.ok_or(InvalidForm::missing_field("Description"))?)
    }
}

#[derive(Debug, Snafu)]
pub enum InvalidForm {
    #[snafu(display("Missing field: {}", field_name))]
    MissingField {
        field_name: String,
    },

    #[snafu(display("Invalid value in field: {}", field_name))]
    InvalidValue {
        field_name: String,
    },
}

impl InvalidForm {
    pub fn missing_field(name: &str) -> InvalidForm {
        InvalidForm::MissingField { field_name: name.to_string() }
    }

    pub fn invalid_value(name: &str) -> InvalidForm {
        InvalidForm::InvalidValue { field_name: name.to_string() }
    }
}
