#[macro_use] pub mod respgen;
pub mod forms;
pub mod calls;

use std::collections::HashMap;

use diesel::prelude::*;
use tera::Context;
use warp::{filters::BoxedFilter, path, reject::{custom, Reject}, reply::{self, Html, Reply}, Filter, Rejection, multipart::FormData};

use crate::auth;
use crate::db_aux;
use crate::models;
use crate::schema;
use crate::search;
use crate::thumbgen;
use crate::upload;
use crate::validation;
use crate::{process_tags, MEDIA_DIR, TERA, POOL};

use respgen::*;

fn attachment_pretemplate(att: models::DbAttachment) -> Result<(models::AttKind, String, Option<String>), std::io::Error> {
    use models::AttKind;

    let string = match att.kind {
        AttKind::Text => {
            std::fs::read_to_string(MEDIA_DIR.join(att.filename))?
        },
        _ => format!("/media/{}", att.filename),
    };

    Ok((att.kind, string, att.orig_name))
}

#[derive(Debug)]
struct TemplateError;
impl Reject for TemplateError {}

async fn render(page: &str, ctx: &Context) -> Result<String, Rejection> {
    TERA.render(page, ctx).or(Err(custom(TemplateError)))
}

/// GET /
pub async fn page_root(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (_, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    let payload = render("root.html", &ctx).await?;

    Ok(Box::new(reply::html(payload)))
}

/// GET /login
pub async fn page_login() -> Result<Html<String>, Rejection> {
    let ctx = Context::new();
    let payload = render("login.html", &ctx).await?;
    Ok(reply::html(payload))
}

/// GET /logout
pub async fn page_logout(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (conn, _, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Delete the session
    auth::delete_session(&conn, &token_cookie.unwrap());

    // Erase client's cookie and redirect to login
    let cookie = "token=; Path=/; Expires=-1".to_string();
    let resp = cookie_redir("/login", &cookie);

    resp
}

/// GET /authors
pub async fn page_authors(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbNamespace;
    use schema::namespaces;

    // Setup database connection, validate token, & verify access permission
    let (conn, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Get orphans
    let orphans = db_aux::orphaned_authors(&conn).unwrap();

    // Get namespace list
    let all_namespaces: Vec<DbNamespace> = namespaces::table.order(namespaces::name.asc())
        .load::<DbNamespace>(&conn)
        .unwrap();

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("orphans", &orphans);
    ctx.insert("all_namespaces", &all_namespaces);
    let payload = render("authors.html", &ctx).await?;
    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /users
pub async fn page_users(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use crate::models::{DbUser, PermissionLevel};
    use crate::schema::users;

    // Setup database connection, validate token, & verify access permission
    let (conn, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyAdmin);

    // Get user list
    let all_users: Vec<(i32, String, PermissionLevel)> = users::table.order(users::id.asc())
        .load::<DbUser>(&conn)
        .unwrap()
        .into_iter()
        .map(|u| (u.id, u.username, u.permission))
        .collect();

    // Render
    let mut ctx = Context::new();
    ctx.insert("all_users", &all_users);
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    let payload = render("users.html", &ctx).await?;

    Ok(Box::new(reply::html(payload)))
}

/// GET /namespaces
pub async fn page_namespaces(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use crate::models::DbNamespace;
    use crate::schema::namespaces;

    // Setup database connection, validate token, & verify access permission
    let (conn, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyAdmin);

    // Get namespace list
    let all_namespaces: Vec<DbNamespace> = namespaces::table.order(namespaces::id.asc())
        .load::<DbNamespace>(&conn)
        .unwrap();

    // Render
    let mut ctx = Context::new();
    ctx.insert("all_namespaces", &all_namespaces);
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    let payload = render("namespaces.html", &ctx).await?;

    Ok(Box::new(reply::html(payload)))
}

/// GET /entry/new
pub async fn page_entry_new(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (_, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    let payload = render("new_entry.html", &ctx).await?;

    Ok(Box::new(reply::html(payload)))
}

/// GET /entry/<entryid>
pub async fn page_entry(entry_id: i32, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::{AttKind, DbAttachment, DbEntry, DbTagLink};
    use schema::{attachments, tag_links};

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get entry
    let entry = DbEntry::by_id(&conn, entry_id);
    let entry = match entry {
        Ok(e) => e,
        Err(diesel::result::Error::NotFound) => {
            return Ok(not_found_msg(&opt_user, Some("entry")).await)
        }
        Err(e) => {
            error!("500 /entry/{entry_id}: Err loading entry by ID: {e}");
            return Ok(ise_msg(&opt_user).await)
        }
    };

    // Get tags
    let tag_list = tag_links::table.filter(tag_links::entry.eq(entry_id))
        .order(tag_links::tag.asc())
        .load::<DbTagLink>(&conn);
    let tag_list: Vec<String> = match tag_list {
        Ok(v) => v.into_iter().map(|t| t.tag).collect(),
        Err(e) => {
            error!("500 /entry/{entry_id}: Err getting tags: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Get attachments & process for template
    let att_list = attachments::table.filter(attachments::entry.eq(entry_id))
        .order(attachments::position.asc())
        .load::<DbAttachment>(&conn);
    let att_list: Vec<(AttKind, String, Option<String>)> = match att_list {
        Ok(v) => {
            let v: Vec<_> = v.into_iter()
                .map(|a| attachment_pretemplate(a))
                .collect();
            for r in v.iter() {
                if let Err(e) = r {
                    // TODO describe, comment this area in general; formerly "Error loading text attachment"
                    error!("500 /entry/{entry_id}: {e}");
                    return Ok(ise_msg(&opt_user).await)
                }
            }
            v.into_iter()
                .map(|r| r.unwrap())
                .collect()
        }
        Err(e) => {
            error!("500 /entry/{entry_id}: Err loading attachment list: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Get collections
    let collections_list = db_aux::entry_collections(&conn, entry_id);
    let collections_list = match collections_list {
        Ok(v) => v,
        Err(e) => {
            error!("500 /entry/{entry_id}: Err loading collection list: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Get author
    let author_info = entry.get_author_info(&conn);
    let author_info = match author_info {
        Ok(a) => a,
        Err(e) => {
            error!("500 /entry/{entry_id}: Err loading author info: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("entry", &entry);
    ctx.insert("tags", &tag_list);
    ctx.insert("attachments", &att_list);
    ctx.insert("collections", &collections_list);
    ctx.insert("author", &author_info.as_ref().map(|a| &a.0));
    ctx.insert("namespace", &author_info.as_ref().map(|a| &a.1));
    let payload = render("entry.html", &ctx).await?;

    Ok(Box::new(reply::html(payload)))
}

/// GET /collections
pub async fn page_collections(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (conn, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Get orphans
    let orphans = db_aux::orphaned_collections(&conn).unwrap();

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("orphans", &orphans);
    let payload = render("collections.html", &ctx).await?;
    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /collection/<collectionid>
pub async fn page_collection(collection_id: i32, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbCollection;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get collection
    let collection = DbCollection::by_id(&conn, collection_id);
    let collection = match collection {
        Ok(c) => c,
        Err(diesel::result::Error::NotFound) => {
            return Ok(not_found_msg(&opt_user, Some("collection")).await)
        }
        Err(e) => {
            error!("500 /collection/{collection_id}: Err loading collection by ID: {e}");
            return Ok(ise_msg(&opt_user).await)
        }
    };

    // Get entries
    let entries_list = db_aux::collection_entries(&conn, collection_id);
    let entries_list = match entries_list {
        Ok(v) => v,
        Err(e) => {
            error!("500 /collection/{collection_id}: Err loading entry list: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("collection", &collection);
    ctx.insert("entries", &entries_list);

    let payload = render("collection.html", &ctx).await?;

    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /search
pub async fn page_search(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (_, _, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    let payload = render("search.html", &ctx).await?;

    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /tag/<tagname>
pub async fn page_tag(tag_name: String, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbTag;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get tag info
    let tag = DbTag::by_name(&conn, &tag_name);
    let tag = match tag {
        Err(diesel::result::Error::NotFound) => {
            return Ok(not_found_msg(&opt_user, Some("tag")).await)
        },
        Err(e) => {
            error!("500 /tag/{tag_name}: Err loading tag by name: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
        Ok(t) => t,
    };

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("tag", &tag);
    let payload = render("tag.html", &ctx).await?;

    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /tags
pub async fn page_tags(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbTagStat;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get tag stats (i.e., counts)
    let stats = DbTagStat::all(&conn);
    let stats = match stats {
        Ok(v) => v,
        Err(e) => {
            error!("500 /tags: Err loading tag stats: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("stats", &stats);
    let payload = render("tags.html", &ctx).await?;

    Ok(Box::new(warp::reply::html(payload)))
}

/// GET /random
pub async fn page_random(token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbEntry;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get random entry
    let entry = DbEntry::random(&conn);
    let entry = match entry {
        Ok(e) => e,
        Err(e) => {
            error!("500 /random: Err loading entry: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    redir(format!("/entry/{}", entry.id))
}

/// POST /login/do
pub async fn req_login(form: forms::Login) -> Result<Box<dyn Reply>, Rejection> {
    // Extract form data
    let (username, password) = match form.unpack() {
        Ok(a) => a,
        Err(e) => return Ok(bad_form_msg(&None, e).await),
    };

    // Attempt login
    let conn = POOL.get().unwrap();
    let session = match auth::create_session(&conn, &username, &password) {
        None => return redir("/login"),
        Some(s) => s,
    };

    // Redirect to root w/ token cookie
    let cookie = format!("token={}; Path=/", session.token);
    let resp = cookie_redir("/", &cookie);

    resp
}

/// POST /author/new
pub async fn req_author_new(form: forms::AuthorCreate, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::{DbNamespace, InsAuthor};

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyAdmin);

    // Extract form data
    let (name, namespace) = match form.unpack() {
        Ok(a) => a,
        Err(e) => return Ok(bad_form_msg(&None, e).await),
    };

    // Ensure name is legal
    let name = match validation::validate_author_name(name) {
        Ok(n) => n,
        Err(_) => return Ok(generic_message(&opt_user, "Error", "Error", "Invalid author name", http::StatusCode::BAD_REQUEST).await),
    };

    // Ensure namespace exists
    let ns = DbNamespace::by_id(&conn, namespace);
    match ns {
        Ok(_) => {},
        Err(diesel::result::Error::NotFound) => {
            return Ok(generic_message(&opt_user, "Error", "Error", "Invalid namespace ID", http::StatusCode::BAD_REQUEST).await)
        },
        Err(e) => {
            error!("500 /author/new: Err verifying namespace: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Insert
    let ins_author = InsAuthor {
        name,
        namespace,
    };
    let author = ins_author.insert(&conn);
    let _ = match author {
        Ok(a) => a,
        Err(diesel::result::Error::DatabaseError(diesel::result::DatabaseErrorKind::UniqueViolation, _)) => {
            return Ok(generic_message(&opt_user, "Error", "Error", "Author name already exists in this namespace", http::StatusCode::CONFLICT).await)
        },
        Err(e) => {
            error!("500 /author/new: Err inserting author: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    redir("/authors")
}

/// POST /author/search
pub async fn req_author_search(query: calls::req::SearchAuthor, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbAuthor;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Perform search
    let results = DbAuthor::with_name_like(&conn, &query.query);
    let results = match results {
        Err(e) => {
            error!("500 /author/search: Err performing search: {}", e);
            return Ok(ise_msg(&opt_user).await)
        },
        Ok(r) => r,
    };

    // Generate response
    let resp = calls::resp::SearchAuthor {
        results,
    };

    Ok(Box::new(warp::reply::json(&resp)))
}

/// POST /user/update/<userid>
pub async fn req_user_update(user_id: i32, form: forms::UserUpdate, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbUser;
    use schema::users;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyAdmin);

    // Get preexisting user information
    let old_user = users::table.filter(users::id.eq(user_id))
        .first::<DbUser>(&conn);
    let old_user = match old_user {
        Err(e) => {
            error!("500 /user/update/{user_id}: Err getting user by ID: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
        Ok(u) => u,
    };

    // Extract form data
    let (username, permissions) = match form.unpack() {
        Ok(t) => t,
        Err(e) => return Ok(bad_form_msg(&opt_user, e).await),
    };

    // Validate form data
    let username_update = match validation::validate_username(username) {
        // Valid username
        Ok(u) => Some(u),

        // No new username specified, do not attempt to update
        Err(validation::ValidateUsernameError::LengthZero) => None,

        // Invalid username specified, return error
        Err(validation::ValidateUsernameError::InvalidCharacter(c)) => {
            let message = format!("Requested username contains disallowed character \"{}\"", c);
            return Ok(generic_message(&opt_user, "Error", "Error", &message, http::StatusCode::BAD_REQUEST).await);
        }
    };

    // Make changes
    let transaction_result = conn.transaction::<(), diesel::result::Error, _>(|| {
        // Username
        if let Some(username) = username_update {
            if username != old_user.username {
                let _ = diesel::update(users::table.find(user_id))
                    .set(users::username.eq(username))
                    .execute(&conn)?;
            }
        }

        // Permissions
        if permissions != old_user.permission {
            let _ = diesel::update(users::table.find(user_id))
                .set(users::permission.eq(permissions))
                .execute(&conn)?;
        }

        Ok(())
    });

    match transaction_result {
        Ok(()) => Ok(generic_message(&opt_user, "Success", "Success", "User info successfully updated", http::StatusCode::OK).await),
        Err(diesel::result::Error::DatabaseError(diesel::result::DatabaseErrorKind::UniqueViolation, _)) => {
            Ok(generic_message(&opt_user, "Error", "Error", "Username is already taken", http::StatusCode::CONFLICT).await)
        }
        Err(e) => {
            error!("500 /user/update/{user_id}: Err in user update transaction: {e}");
            return Ok(ise_msg(&opt_user).await)
        }
    }
}

/// POST /namespace/create
pub async fn req_namespace_create(form: forms::NamespaceCreate, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::InsNamespace;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyAdmin);

    let new_name = match form.unpack() {
        Ok(n) => n,
        Err(e) => return Ok(bad_form_msg(&opt_user, e).await),
    };

    let ins_namespace = InsNamespace {
        name: new_name,
    };

    let res = ins_namespace.insert(&conn);
    match res {
        Ok(_) => return redir("/namespaces"),
        Err(diesel::result::Error::DatabaseError(diesel::result::DatabaseErrorKind::UniqueViolation, _)) => {
            Ok(generic_message(&opt_user, "Error", "Error", "Namespace name is already taken", http::StatusCode::CONFLICT).await)
        }
        Err(e) => {
            error!("500 /namespace/create: Err creating namespace: {e}");
            return Ok(ise_msg(&opt_user).await)
        }
    }
}

/// POST /entry/new/do
pub async fn req_entry_new(form: FormData, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::{DbEntry, InsAttachment, InsTag, InsTagLink};

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Process the form including its attachments
    let inc_entry = upload::process_new_entry(form).await;
    if let Err(e) = inc_entry {
        let (message, status) = (&e.to_string()[..], e.http_status());
        return Ok(generic_message(&opt_user, "Error", "Error creating entry", message, status).await)
    };
    let upload::IncompleteEntry { ins_entry, tag_vec, att_vec } = inc_entry.unwrap();

    let transaction_result = conn.transaction::<DbEntry, diesel::result::Error, _>(|| {
        // Insert the entry
        let entry: DbEntry = ins_entry.insert(&conn)?;

        for tag in tag_vec {
            // Create tag if it doesn't exist
            let ins_tag = InsTag {
                name: tag.clone(),
                description: None,
            };
            ins_tag.insert_or_not(&conn)?;

            // Tag this entry
            let ins_tag_link = InsTagLink {
                entry: entry.id,
                tag: tag.clone(),
            };
            ins_tag_link.insert(&conn)?;
        }

        // Insert all attachments
        for a in att_vec {
            let ins_attachment = InsAttachment {
                entry: entry.id,
                position: a.position,
                filename: a.filename,
                kind: a.kind,
                orig_name: a.orig_name,
            };
            ins_attachment.insert(&conn)?;
        }

        Ok(entry)
    });

    // Extract new entry
    let entry = match transaction_result {
        Ok(e) => e,
        Err(e) => {
            error!("500 /entry/new/do: Err in transaction: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Generate thumbnail
    match thumbgen::gen_thumb(&entry, &conn) {
        Ok(()) => info!("Generated thumbnail for entry {}", entry.id),
        Err(e) => error!("Error generating thumbnail for entry {}: {}", entry.id, e),
    };

    redir(format!("/entry/{}", entry.id))
}

/// POST /collection/new
pub async fn req_collection_new(form: forms::NewCollection, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::InsCollection;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Get new collection name
    let new_name = match form.unpack() {
        Ok(n) => n,
        Err(e) => return Ok(bad_form_msg(&opt_user, e).await),
    };

    // Insert
    let ins_collection = InsCollection { name: new_name };
    let collection = ins_collection.insert(&conn);
    let collection = match collection {
        Ok(c) => c,
        Err(e) => {
            error!("500 /collection/new: Err inserting collection: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    redir(format!("/collection/{}", collection.id))
}

/// POST /collection/<collectionid>/append
pub async fn req_collection_append(collection_id: i32, form: forms::AppendCollection, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    let entry_id = match form.unpack() {
        Ok(e) => e,
        Err(e) => return Ok(bad_form_msg(&opt_user, e).await),
    };

    let res = db_aux::collection_append_entry(&conn, collection_id, entry_id);
    match res {
        Ok(()) => {
            redir(format!("/collection/{}", collection_id))
        },
        Err(diesel::result::Error::DatabaseError(diesel::result::DatabaseErrorKind::UniqueViolation, _)) => {
            Ok(generic_message(&opt_user, "Error", "Error", "Entry can only appear in collection once", http::StatusCode::BAD_REQUEST).await)
        },
        Err(e) => {
            error!("500 /collection/{collection_id}/append: Err appending entry to collection: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    }
}

/// GET /search/results
pub async fn req_search(form: HashMap<String, String>, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, user) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyUser);

    // Get tags
    let tags = match form.get("tags") {
        Some(s) => process_tags(s),
        None => return Ok(generic_message(&opt_user, "Error", "Error", "Search requires tags", http::StatusCode::BAD_REQUEST).await),
    };

    let results = search::SearchRequest::new()
        .with_tags(tags)
        .execute(&conn);
    let results = match results {
        Ok(v) => v,
        Err(e) => {
            error!("500 /search/results: Err executing search: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
    };

    // Render
    let mut ctx = Context::new();
    ctx.insert("username", &user.username);
    ctx.insert("permission", &user.permission);
    ctx.insert("results", &results);
    ctx.insert("form", &form);
    let payload = render("results.html", &ctx).await?;

    Ok(Box::new(warp::reply::html(payload)))
}

/// POST /tag/<tagname>/update
pub async fn req_tag_update(tag_name: String, form: forms::UpdateTag, token_cookie: Option<String>) -> Result<Box<dyn Reply>, Rejection> {
    use models::DbTag;

    // Setup database connection, validate token, & verify access permission
    let (conn, opt_user, _) = auth_etc!(POOL, token_cookie, auth::AccessPolicy::AnyWrite);

    // Get tag info
    let tag = DbTag::by_name(&conn, &tag_name);
    let tag = match tag {
        Err(diesel::result::Error::NotFound) => {
            return Ok(not_found_msg(&opt_user, Some("tag")).await)
        },
        Err(e) => {
            error!("500 /tag/{tag_name}/update: Err loading tag by name: {e}");
            return Ok(ise_msg(&opt_user).await)
        },
        Ok(t) => t,
    };

    // Extract data from form & update
    let new_description = match form.unpack() {
        Ok(e) => e,
        Err(e) => return Ok(bad_form_msg(&opt_user, e).await),
    };
    let new_description = match new_description.trim() {
        "" => None,
        s => Some(s.to_string()),
    };

    let result = tag.set_description(&conn, new_description);
    match result {
        Ok(()) => {
            redir(format!("/tag/{tag_name}"))
        }
        Err(e) => {
            error!("500 /tag/{tag_name}/update: Err setting description: {e}");
            return Ok(ise_msg(&opt_user).await)
        }
    }
}

pub async fn routes() -> BoxedFilter<(impl Reply,)> {
    let static_files = warp::get()
        .and(warp::path("static"))
        .and(warp::fs::dir("static"));

    let media_files = warp::get()
        .and(warp::path("media"))
        .and(warp::fs::dir(MEDIA_DIR.to_owned()));

    let get_root = warp::get()
        .and(warp::path::end())
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_root);

    let get_login = warp::get()
        .and(path!("login"))
        .and_then(page_login);

    let get_logout = warp::get()
        .and(path!("logout"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_logout);

    let get_authors = warp::get()
        .and(path!("authors"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_authors);

    let get_users = warp::get()
        .and(path!("users"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_users);

    let get_namespaces = warp::get()
        .and(path!("namespaces"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_namespaces);

    let get_entry_new = warp::get()
        .and(path!("entry" / "new"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_entry_new);

    let get_entry = warp::get()
        .and(path!("entry" / i32))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_entry);

    let get_collections = warp::get()
        .and(path!("collections"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_collections);

    let get_collection = warp::get()
        .and(path!("collection" / i32))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_collection);

    let get_search = warp::get()
        .and(path!("search"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_search);

    let get_tag = warp::get()
        .and(path!("tag" / String))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_tag);

    let get_tags = warp::get()
        .and(path!("tags"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_tags);

    let get_random = warp::get()
        .and(path!("random"))
        .and(warp::filters::cookie::optional("token"))
        .and_then(page_random);

    let post_login_do = warp::post()
        .and(path!("login" / "do"))
        .and(warp::body::form())
        .and_then(req_login);

    let post_author_new = warp::post()
        .and(path!("author" / "new"))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_author_new);

    let post_author_search = warp::post()
        .and(path!("author" / "search"))
        .and(warp::body::json())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_author_search);

    let post_user_update = warp::post()
        .and(path!("user" / "update" / i32))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_user_update);

    let post_namespace_create = warp::post()
        .and(path!("namespace" / "create"))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_namespace_create);

    let post_entry_new_do = warp::post()
        .and(path!("entry" / "new" / "do"))
        .and(warp::multipart::form().max_length(500_000_000))
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_entry_new);

    let post_collection_new = warp::post()
        .and(path!("collection" / "new"))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_collection_new);

    let post_collection_append = warp::post()
        .and(path!("collection" / i32 / "append"))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_collection_append);

    let get_search_do = warp::get()
        .and(path!("search" / "results"))
        .and(warp::query())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_search);

    let post_tag_update = warp::post()
        .and(path!("tag" / String / "update"))
        .and(warp::body::form())
        .and(warp::filters::cookie::optional("token"))
        .and_then(req_tag_update);

    let joined = warp::any()
        .and(static_files
             .or(media_files)
             .or(get_root)
             .or(get_login)
             .or(get_logout)
             .or(get_authors)
             .or(get_users)
             .or(get_namespaces)
             .or(get_entry_new)
             .or(get_entry)
             .or(get_collections)
             .or(get_collection)
             .or(get_search)
             .or(get_tag)
             .or(get_tags)
             .or(get_random)
             .or(post_login_do)
             .or(post_author_new)
             .or(post_author_search)
             .or(post_user_update)
             .or(post_namespace_create)
             .or(post_entry_new_do)
             .or(post_collection_new)
             .or(post_collection_append)
             .or(get_search_do)
             .or(post_tag_update));

    joined.boxed()
}
