use std::str::FromStr;

use http::uri::Uri;
use tera::Context;
use warp::{reply, Reply};

use crate::models;
use crate::web;

macro_rules! ensure_allowed {
    ( $e:expr, $f:expr ) => {
        match $e.determine($f) {
            auth::AccessDecision::Allow => {},
            auth::AccessDecision::DenyRedirLogin => return redir("/login"),
            auth::AccessDecision::DenyOutright => return Ok(forbidden_msg($f).await),
        }
    };
}

macro_rules! auth_etc {
    ( $e:expr, $f:expr, $g:expr ) => {{
        let conn = $e.get().unwrap();
        let opt_user = auth::validate_session(&conn, &$f);
        ensure_allowed!($g, &opt_user);
        let user = opt_user.clone().unwrap();

        (conn, opt_user, user)
    }}
}

/// Generate 302 redirect
pub fn redir<T: ToString>(path: T) -> Result<Box<dyn Reply>, warp::Rejection> {
    let string = path.to_string();
    let uri = Uri::from_str(&string).unwrap();
    let resp = warp::redirect::found(uri);
    Ok(Box::new(resp))
}

/// Generate 302 redirect with a cookie
pub fn cookie_redir<T: ToString>(path: T, cookie: T) -> Result<Box<dyn Reply>, warp::Rejection> {
    let string = path.to_string();
    let uri = Uri::from_str(&string).unwrap();
    let resp = warp::reply::with_header(warp::redirect::found(uri), http::header::SET_COOKIE, cookie.to_string());
    Ok(Box::new(resp))
}

/// Generate a boilerplate error page
pub async fn generic_message(opt_user: &Option<models::DbUser>, title: &str, summary: &str, message: &str, status: http::StatusCode) -> Box<dyn Reply> {
    let (opt_username, opt_permission) = match opt_user {
        None => (None, None),
        Some(u) => (Some(u.username.clone()), Some(u.permission.clone())),
    };

    let mut ctx = Context::new();
    ctx.insert("title", &title);
    ctx.insert("summary", &summary);
    ctx.insert("message", &message);
    ctx.insert("username", &opt_username);
    ctx.insert("permission", &opt_permission);

    let payload = super::render("message.html", &ctx).await.unwrap();
    let reply = reply::with_status(reply::html(payload), status);

    Box::new(reply)
}

/// Nearly-static 403 page, inserting only the user info
pub async fn forbidden_msg(opt_user: &Option<models::DbUser>) -> Box<dyn Reply> {
    let title = "Error";
    let summary = "403 Forbidden";
    let message = "You do not have access to this resource";
    generic_message(opt_user, title, summary, message, http::StatusCode::FORBIDDEN).await
}

/// Generate a 404 page
pub async fn not_found_msg<T: std::fmt::Display>(opt_user: &Option<models::DbUser>, item: Option<T>) -> Box<dyn Reply> {
    let title = "Error";
    let summary = "404 Not Found";
    let message = match &item {
        Some(t) => format!("Requested {t} does not exist"),
        None => String::from("Requested resource does not exist"),
    };
    generic_message(opt_user, title, summary, &message, http::StatusCode::NOT_FOUND).await
}

/// Generate a 500 page
pub async fn ise_msg(opt_user: &Option<models::DbUser>) -> Box<dyn Reply> {
    let title = "Error";
    let summary = "500 Internal Server Error";
    let message = "Unexpected error loading or processing this request";
    generic_message(opt_user, title, summary, message, http::StatusCode::INTERNAL_SERVER_ERROR).await
}

/// Generate a 400 page due to form validation
pub async fn bad_form_msg(opt_user: &Option<models::DbUser>, err: web::forms::InvalidForm) -> Box<dyn Reply> {
    let title = "Error";
    let summary = "400 Bad Request";
    let message = err.to_string();
    generic_message(opt_user, title, summary, &message, http::StatusCode::BAD_REQUEST).await
}
