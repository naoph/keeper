#![recursion_limit = "256"]

#[macro_use] extern crate cmd_lib;
#[macro_use] extern crate diesel;
#[macro_use] extern crate lazy_static;
#[macro_use] extern crate log;

use std::error::Error;
use std::path::PathBuf;

use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use rand::Rng;
use tera::Tera;

mod auth;
mod db_aux;
mod models;
mod schema;
mod search;
mod thumbgen;
mod upload;
mod validation;
mod web;

lazy_static! {
    pub static ref TERA: Tera = {
        let path = concat!(env!("CARGO_MANIFEST_DIR"), "/templates/*.html");
        let tera = match Tera::new(path) {
            Ok(t) => t,
            Err(e) => {
                println!("Failed to set up templating: {}", e);
                std::process::exit(1);
            }
        };
        tera
    };

    // TODO make this configurable
    pub static ref MEDIA_DIR: PathBuf = {
        let home = match dirs::home_dir() {
            Some(d) => d,
            None => {
                println!("Failed to locate home directory");
                std::process::exit(1);
            }
        };
        let media = home.join("keeper_media");
        if !media.exists() {
            match std::fs::create_dir(&media) {
                Ok(_) => {},
                Err(e) => {
                    println!("Failed to create media directory: {}", e);
                    std::process::exit(1);
                }
            }
        }
        let thumbs = media.join("thumbnails");
        if !thumbs.exists() {
            match std::fs::create_dir(&thumbs) {
                Ok(_) => {},
                Err(e) => {
                    println!("Failed to create thumbnail directory: {}", e);
                    std::process::exit(1);
                }
            }
        }
        media
    };

    pub static ref POOL: PgPool = {
        match setup_database() {
            Ok(t) => t,
            Err(e) => {
                println!("Failed to connect to database: {}", e);
                std::process::exit(1);
            },
        }
    };
}

// TODO make this configurable
pub static THUMB_SIZE: u32 = 262;

type PgPool = r2d2::Pool<ConnectionManager<PgConnection>>;

fn setup_database() -> Result<PgPool, Box<dyn Error>> {
    let url = std::env::var("DATABASE_URL")?;
    let manager = ConnectionManager::<PgConnection>::new(url);
    let pool = r2d2::Pool::builder().build(manager)?;
    Ok(pool)
}

/// Create admin user if none exists
fn initialize_db(conn: &PgConnection) {
    use models::*;
    use schema::users::dsl::*;

    let admin = users.filter(username.eq("admin"))
        .limit(1)
        .load::<DbUser>(conn)
        .unwrap()
        .pop();

    if let Some(admin) = admin {
        // Admin user exists
        if !admin.allowed_admin() {
            info!("User `admin` exists but does not have admin permission");
        }
    } else {
        // Admin user doesn't exist
        let password_bytes = rand::thread_rng().gen::<[u8; 12]>();
        let _password = base64::encode(password_bytes);

        // Overwrite password to "password" for testing convenience
        // TODO ^
        let password = "password".to_string();

        let admin = InsUser::create("admin", &password, models::PermissionLevel::Admin);
        diesel::insert_into(schema::users::table)
            .values(&admin)
            .get_result::<DbUser>(conn)
            .unwrap();
        info!("User `admin` created with password: {}", password);
    };
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::init();
    dotenv::dotenv()?;

    let conn = POOL.get()?;
    initialize_db(&conn);

    let routes = web::routes().await;

    Ok(warp::serve(routes).run(([127, 0, 0, 1], 8080)).await)
}

/// Get a list of tags from a space-separated string, trimming leading/trailing whitespace &
/// filtering empty tags
pub fn process_tags<T: ToString>(tag_string: T) -> Vec<String> {
    // Get list of tags from string
    let mut tags: Vec<String> = tag_string.to_string().split(" ")
        .filter(|t| t != &"")
        .map(|t| String::from(t.trim()))
        .collect();

    tags.sort();
    tags.dedup();

    tags
}
