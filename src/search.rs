use diesel::prelude::*;

use crate::models::{DbEntry, DbEntryWithTagArray};
use crate::schema::entries_with_tag_array;

/// A search request, encompassing all fields and options available
pub struct SearchRequest {
    tags: Vec<String>,
}

impl SearchRequest {
    /// An empty request; currently returns every entry by default
    pub fn new() -> SearchRequest {
        SearchRequest {
            tags: Vec::new(),
        }
    }

    /// Set the request's tags field to a specified value
    pub fn with_tags(mut self, tags: Vec<String>) -> SearchRequest {
        self.tags = tags;
        self
    }

    /// Perform the search, returning entries
    pub fn execute(&self, conn: &PgConnection) -> Result<Vec<DbEntry>, diesel::result::Error> {
        // Currently uses a view due to Diesel's lack of support for HAVING
        let filtered: Vec<DbEntryWithTagArray> = entries_with_tag_array::table
            .filter(entries_with_tag_array::tag_list.contains(&self.tags))
            .order(entries_with_tag_array::id.desc())
            .load(conn)?;

        let out = filtered.into_iter()
            .map(|e| e.to_pair().0)
            .collect();

        Ok(out)
    }
}
