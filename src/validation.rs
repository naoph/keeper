use std::error::Error;

/// Ensure a new username is valid (non-zero length; alphanumeric)
pub fn validate_username<T: ToString>(username: T) -> Result<String, ValidateUsernameError> {
    // Ensure length isn't zero
    let username = username.to_string();
    if username.len() == 0 {
        return Err(ValidateUsernameError::LengthZero);
    }

    // Ensure no non-alphanumeric characters
    for c in username.chars() {
        if !c.is_ascii_alphanumeric() {
            return Err(ValidateUsernameError::InvalidCharacter(c));
        }
    }

    Ok(username)
}

#[derive(Debug)]
pub enum ValidateUsernameError {
    LengthZero,
    InvalidCharacter(char),
}

impl std::fmt::Display for ValidateUsernameError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            &ValidateUsernameError::LengthZero => write!(f, "Username length cannot be zero"),
            &ValidateUsernameError::InvalidCharacter(c) => write!(f, "Username contains illegal character {}", c),
        }
    }
}

impl Error for ValidateUsernameError {}

/// Ensure a new author name is valid (non-zero length; alphanumeric, spaces, underscores, periods, hyphens)
pub fn validate_author_name<T: ToString>(author_name: T) -> Result<String, ValidateAuthorNameError> {
    // Ensure length isn't zero
    let author_name = author_name.to_string();
    if author_name.len() == 0 {
        return Err(ValidateAuthorNameError::LengthZero);
    }

    // Ensure no illegal characters
    for c in author_name.chars() {
        if !c.is_ascii_alphanumeric() && (c != ' ') && (c != '_') && (c != '.') && (c != '-') {
            return Err(ValidateAuthorNameError::InvalidCharacter(c));
        }
    }

    Ok(author_name)
}

#[derive(Debug)]
pub enum ValidateAuthorNameError {
    LengthZero,
    InvalidCharacter(char),
}

impl std::fmt::Display for ValidateAuthorNameError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            &ValidateAuthorNameError::LengthZero => write!(f, "Author name length cannot be zero"),
            &ValidateAuthorNameError::InvalidCharacter(c) => write!(f, "Author name contains illegal character {}", c),
        }
    }
}

impl Error for ValidateAuthorNameError {}
