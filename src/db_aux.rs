use diesel::dsl;
use diesel::prelude::*;
use serde::Serialize;

use crate::models::*;
use crate::schema::*;

/// All data necessary to render one collection in an entry's collections box
#[derive(Debug, Serialize)]
pub struct EntryCollection {
    pub collection: DbCollection,
    pub position: i32,
    pub quantity: i64,
    pub prev: Option<DbEntry>,
    pub next: Option<DbEntry>,
}

/// Generate EntryCollection for each collection a given entry belongs to
pub fn entry_collections(conn: &PgConnection, entry_id: i32) -> Result<Vec<EntryCollection>, diesel::result::Error> {
    // Get relevant collections & their links to this entry
    let list: Vec<(DbCollectionLink, DbCollection)> = collection_links::table
        .inner_join(collections::table)
        .filter(collection_links::entry.eq(entry_id))
        .load(conn)?;

    let mut out = Vec::new();

    for (this_link, this_collection) in list {
        // Get quantity of entries in collection
        let quantity: i64 = collection_links::table
            .filter(collection_links::collection.eq(this_collection.id))
            .count()
            .get_result(conn)?;

        // Get previous entry if applicable
        let prev: Option<DbEntry> = if this_link.position == 1 {
            None
        } else {
            collection_links::table
                .inner_join(entries::table)
                .filter(collection_links::collection.eq(this_collection.id))
                .filter(collection_links::position.eq(this_link.position - 1))
                .first::<(DbCollectionLink, DbEntry)>(conn)
                .ok()
                .map(|(_, e)| e)
        };

        // Get next entry if applicable
        let next: Option<DbEntry> = if this_link.position as i64 == quantity {
            None
        } else {
            collection_links::table
                .inner_join(entries::table)
                .filter(collection_links::collection.eq(this_collection.id))
                .filter(collection_links::position.eq(this_link.position + 1))
                .first::<(DbCollectionLink, DbEntry)>(conn)
                .ok()
                .map(|(_, e)| e)
        };

        // Push
        out.push(EntryCollection {
            collection: this_collection,
            position: this_link.position,
            quantity,
            prev,
            next,
        });
    }

    Ok(out)
}

/// All data necessary to render one entry on a collection page
#[derive(Debug, Serialize)]
pub struct CollectionEntry {
    pub entry: DbEntry,
    pub position: i32,
}

/// Generate CollectionEntry for each entry in a given collection
pub fn collection_entries(conn: &PgConnection, collection_id: i32) -> Result<Vec<CollectionEntry>, diesel::result::Error> {
    // Get relevant entries & their links to this collection
    let list: Vec<(DbEntry, DbCollectionLink)> = entries::table
        .inner_join(collection_links::table)
        .filter(collection_links::collection.eq(collection_id))
        .order(collection_links::position.asc())
        .load(conn)?;

    // Assemble CollectionEntry
    let out: Vec<_> = list.into_iter()
        .map(|(e, cl)| CollectionEntry { entry: e, position: cl.position })
        .collect();

    Ok(out)
}

/// Generate list of collections with no entry links
pub fn orphaned_collections(conn: &PgConnection) -> Result<Vec<DbCollection>, diesel::result::Error> {
    // Left join collections with links
    let joined: Vec<(DbCollection, Option<DbCollectionLink>)> = collections::table
        .left_outer_join(collection_links::table)
        .load(conn)?;

    // Filter for collections without links
    let mut orphans = Vec::new();
    for (c, ol) in joined {
        if ol.is_none() {
            orphans.push(c);
        }
    }

    Ok(orphans)
}

/// Generate list of authors with no entry links
pub fn orphaned_authors(conn: &PgConnection) -> Result<Vec<DbAuthor>, diesel::result::Error> {
    // Left join authors with entries
    let joined: Vec<(DbAuthor, Option<DbEntry>)> = authors::table
        .left_outer_join(entries::table)
        .load(conn)?;

    // Filter for authors without entries
    let mut orphans = Vec::new();
    for (a, oe) in joined {
        if oe.is_none() {
            orphans.push(a);
        }
    }

    Ok(orphans)
}

/// Append an entry to a collection
pub fn collection_append_entry(conn: &PgConnection, collection_id: i32, entry_id: i32) -> Result<(), diesel::result::Error> {
    // Collect collection & entry info
    let collection = DbCollection::by_id(conn, collection_id)?;
    let entry = DbEntry::by_id(conn, entry_id)?;

    // Determine position of last entry
    let max_position = collection_links::table
        .filter(collection_links::collection.eq(collection.id))
        .select(dsl::max(collection_links::position))
        .first::<Option<i32>>(conn)?
        .unwrap_or(0);

    // Create link
    let ins_collection_link = InsCollectionLink {
        entry: entry.id,
        collection: collection.id,
        position: max_position + 1,
    };

    // Insert link
    diesel::insert_into(collection_links::table)
        .values(&ins_collection_link)
        .get_result::<DbCollectionLink>(conn)?;

    Ok(())
}
