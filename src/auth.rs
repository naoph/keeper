use diesel::prelude::*;

use crate::models::{DbSession, DbUser, InsSession};
use crate::schema::{sessions, users};

/// Validate a username and password and, if valid, create a new session and return it
pub fn create_session(conn: &PgConnection, username: &String, password: &String) -> Option<DbSession> {
    // Get the user
    let user: DbUser = users::table.filter(users::username.eq(username))
        .first(conn)
        .ok()?;

    // Auth
    if !user.check_password(&password) {
        return None;
    }

    // Create session
    let ins_session = InsSession::new(user.id);
    let session: DbSession = diesel::insert_into(sessions::table)
        .values(&ins_session)
        .get_result(conn)
        .ok()?;

    Some(session)
}

/// Validate a session token and, if valid, return a UserPair
pub fn validate_session(conn: &PgConnection, token: &Option<String>) -> Option<DbUser> {
    // Unwrap token, forwarding if None
    let token = match token {
        None => return None,
        Some(t) => t,
    };

    // Get session
    let session: (DbUser, DbSession) = users::table.inner_join(sessions::table)
        .filter(sessions::token.eq(token))
        .first(conn)
        .ok()?;

    Some(session.0)
}

/// Delete a session (logout), returning whether it was deleted
pub fn delete_session(conn: &PgConnection, token: &String) -> bool {
    // Delete session
    let resp = diesel::delete(sessions::table.filter(sessions::token.eq(token)))
        .execute(conn);

    match resp {
        Ok(1) => true,
        _ => false,
    }
}

/// Represent request restrictions by user, permissions, etc.
pub enum AccessPolicy {
    AnyUser,
    AnyWrite,
    AnyAdmin,
    _OneUser(i32),
}

impl AccessPolicy {
    pub fn determine(self, opt_user: &Option<DbUser>) -> AccessDecision {
        use AccessPolicy::*;
        use AccessDecision::*;

        match (self, opt_user) {
            (AnyUser, Some(_)) => Allow,
            (AnyUser, None) => DenyRedirLogin,
            (AnyWrite, Some(u)) => if u.allowed_write() { Allow } else { DenyOutright },
            (AnyWrite, None) => DenyRedirLogin,
            (AnyAdmin, Some(u)) => if u.allowed_admin() { Allow } else { DenyOutright },
            (AnyAdmin, None) => DenyRedirLogin,
            (_OneUser(i), Some(u)) => if u.id == i { Allow } else {DenyOutright },
            (_OneUser(_), None) => DenyRedirLogin,
        }
    }
}

/// Action to be taken based on access policy & user info
pub enum AccessDecision {
    // Allow the request
    Allow,

    // Deny the request, returning 403
    DenyOutright,

    // Deny the request, redirecting to the login page
    DenyRedirLogin,
}
