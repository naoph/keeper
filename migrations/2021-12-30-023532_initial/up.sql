CREATE TYPE att_kind AS ENUM ('image', 'video', 'text');

CREATE TYPE permission_level AS ENUM ('read', 'write', 'admin');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    username text UNIQUE NOT NULL,
    passhash text NOT NULL,
    permission permission_level NOT NULL
);

CREATE TABLE sessions (
    id SERIAL PRIMARY KEY,
    token text UNIQUE NOT NULL,
    userid integer NOT NULL
);

CREATE TABLE entries (
    id SERIAL PRIMARY KEY,
    title text,
    source text,
    comment text,
    thumbnail text,
    author integer,
    source_is_orig boolean,
    CHECK ((source IS NULL) = (source_is_orig IS NULL))
);

CREATE TABLE attachments (
    id SERIAL PRIMARY KEY,
    entry integer NOT NULL,
    position integer NOT NULL,
    filename text NOT NULL,
    kind att_kind NOT NULL,
    orig_name text
);

CREATE TABLE collections (
    id SERIAL PRIMARY KEY,
    name text NOT NULL
);

CREATE TABLE collection_links (
    id SERIAL PRIMARY KEY,
    entry integer NOT NULL,
    collection integer NOT NULL,
    position integer NOT NULL,
    UNIQUE (entry, collection)
);

CREATE TABLE tags (
    name text PRIMARY KEY,
    description text
);

CREATE TABLE tag_links (
    id SERIAL PRIMARY KEY,
    entry integer NOT NULL,
    tag text NOT NULL,
    UNIQUE (entry, tag)
);

CREATE TABLE namespaces (
    id SERIAL PRIMARY KEY,
    name text UNIQUE NOT NULL
);

CREATE TABLE authors (
    id SERIAL PRIMARY KEY,
    name text NOT NULL,
    namespace integer NOT NULL,
    UNIQUE (name, namespace)
);

CREATE VIEW entries_with_tag_array AS
SELECT entries.*, array_agg(tag_links.tag) AS tag_list
FROM entries INNER JOIN tag_links ON entries.id = tag_links.entry
GROUP BY entries.id;

CREATE VIEW tag_stats AS
SELECT tag, count(*) AS occurrences
FROM tag_links
GROUP BY tag;
